const express = require("express");
const app = express();
app.use(express.json());
const port = 8000;

var mongoose = require("mongoose");
mongoose.connect("mongodb://localhost:27017/{CRUD_Pizza365}", (error) => {
    if(error) throw error;
    console.log("Successfully connected");
});

const drinkModel = require("./app/model/drinkModel");
const voucherModel = require("./app/model/voucherModel");
const orderModel = require("./app/model/orderModel");
const userModel = require("./app/model/userModel");
const drinkRouter = require("./app/routes/drinkRouter");
const vourcherRouter = require("./app/routes/vourcherRouter");
const userRouter = require("./app/routes/userRouter");
const orderRouter = require("./app/routes/orderRouter");

app.use(drinkRouter);
app.use(vourcherRouter);
app.use(userRouter);
app.use(orderRouter);
app.use(drinkModel);
app.use(voucherModel);
app.use(orderModel);
app.use(userModel);

app.listen(port, () => {
    console.log("App listening on port: ", port);
});