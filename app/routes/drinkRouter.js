const express = require("express");
const { getAllDrinks, getDrinkByDrinkId, createDrink, updateDrink, deleteDrink } = require("../controllers/drinkController");
const router = express.Router();

router.get("/drinks", getAllDrinks);
router.get("/drinks/:drinkId", getDrinkByDrinkId);
router.post("/drinks", createDrink);
router.put("/drinks/:drinkId", updateDrink);
router.delete("/drinks/:drinkId", deleteDrink);

module.exports = router;