const express = require("express");
const router = express.Router();

router.get("/users", (req, res) => {
    res.status(200).json({
        message: `Get all users`
    });
});

router.get("/users/:userId", (req, res) => {
    let userId = req.params.userId;
    res.status(200).json({
        message: `Get userId = ${userId}`
    });
});

router.post("/users", (req, res) => {
    res.status(200).json({
        message: `Create user`
    });
});

router.put("/users/:userId", (req, res) => {
    let userId = req.params.userId;
    res.status(200).json({
        message: `Update userId = ${userId}`
    });
});

router.delete("/users/:userId", (req, res) => {
    let userId = req.params.userId;
    res.status(200).json({
        message: `Delete userId = ${userId}`
    });
});

module.exports = router;