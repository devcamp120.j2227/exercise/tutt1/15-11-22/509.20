const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const drink = new Schema({
    _id: mongoose.Types.ObjectId,
    maNuocUong: {
        type: String,
        required: true,
        unique: true
    },
    tenNuocUong: {
        type: String,
        required: true,
    },
    donGia: {
        type: Number,
        required: true
    }
}, {
    timestamps: true
});

module.exports = mongoose.model("drink", drink);