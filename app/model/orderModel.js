const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const order = new Schema({
    _id: mongoose.Types.ObjectId,
    orderCode: {
        type: String,
        unique: true
    },
    pizzaSize: {
        type: String,
        required: true
    },
    pizzaType: {
        type: String,
        required: true
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: "vouchers"
    },
    drink: {
        type: mongoose.Types.ObjectId,
        ref: "drink"
    },
    status: {
        type: String,
        required: true
    },
    ngayTao: {
        type: Date,
        default: new Date()
    },
    ngayCapNhat: {
        type: Date,
        default: new Date()
    }
});

module.exports = mongoose.model("orders", order);