const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const user = new Schema({
    _id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    address: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    orders: [
        {
            type: mongoose.Types.ObjectId,
            ref: "orders"
        }
    ],
    ngayTao: {
        type: Date,
        default: new Date()
    },
    ngayCapNhat: {
        type: Date,
        default: new Date()
    }
});

module.exports = mongoose.model("users", user);