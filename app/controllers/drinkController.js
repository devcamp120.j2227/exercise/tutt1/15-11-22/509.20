const mongoose = require("mongoose");
const drinkModel = require("../model/drinkModel");

const getAllDrinks = (req, res) => {
    // B1: Thu thập dự liệu - bỏ qua

    // B2: Kiểm tra dữ liệu - bỏ qua

    // B3: Xử lý và hiển thị kết quả
    drinkModel.find((error, data) => {
        if(error) {
            return res.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return res.status(200).json({
                message: `Successfully get all drink`,
                drink: data
            });
        }
    });
};

const getDrinkByDrinkId = (req, res) => {
    // B1: Thu thập dữ liệu
    let drinkId = req.params.drinkId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            message: `DrinkId is invalid`
        });
    }
    // B3: Xử lý và hiển thị kết quả
    drinkModel.findById(drinkId, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return res.status(200).json({
                message: `Successfully get drink`,
                drink: data
            });
        }
    });
};

const createDrink = (req, res) => {
    // B1: Thu thập dữ liệu
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if(!body.maNuocUong) {
        return res.status(400).json({
            message: "Mã nước uống is required"
        });
    };
    if(!body.tenNuocUong) {
        return res.status(400).json({
            message: "Tên nước uống is required"
        });
    };
    if(!Number.isInteger(body.donGia) || body.donGia < 0) {
        return res.status(400).json({
            message: "Đơn giá is invalid"
        });
    };
    // B3: Xử lý và trả về kết quả
    console.log("Create drink successfully");
    console.log(body);

    let newDrink = new drinkModel({
        _id: mongoose.Types.ObjectId(),
        maNuocUong: body.maNuocUong,
        tenNuocUong: body.tenNuocUong,
        donGia: body.donGia
    });
    drinkModel.create(newDrink, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return res.status(201).json({
                message: `Successfully created !`,
                drink: data
            });
        }
    });
};

const updateDrink = (req, res) => {
    // B1: Thu thập dữ liệu
    let drinkId = req.params.drinkId;
    let body = req.body;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            message: `DrinkId is invalid`
        });
    }
    if(!body.maNuocUong) {
        return res.status(400).json({
            message: `maNuocUong is required`
        });
    }
    if(!body.tenNuocUong) {
        return res.status(400).json({
            message: `tenNuocUong is required`
        });
    }
    if(!body.donGia) {
        return res.status(400).json({
            message: `donGia is required`
        });
    }
    if(!Number.isInteger(body.donGia)) {
        return res.status(400).json({
            message: `donGia is invalid`
        });
    }
    // B3: Xử lý và hiển thị kết quả
    drinkModel.findByIdAndUpdate(drinkId, body, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return res.status(200).json({
                message: `Successfully update drink`,
                drink: data
            });
        }
    })
};

const deleteDrink = (req, res) => {
    // B1: Thu thập dữ liệu
    let drinkId = req.params.drinkId;
    // B2: Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            message: `DrinkId is invalid`
        });
    }
    // B3: Xử lý và hiển thị kết quả
    drinkModel.findByIdAndDelete(drinkId, (error, data) => {
        if(error) {
            return res.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return res.status(204).json({
                message: `Successfully delete drink`,
                drink: data
            });
        }
    })
};

module.exports = { getAllDrinks, getDrinkByDrinkId, createDrink, updateDrink, deleteDrink }